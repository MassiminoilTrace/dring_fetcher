use std::fmt::Display;


#[derive(Debug)]
pub enum RingError
{
    InvalidCharacter(char),
    TooLong(usize),
    WrongNumberPieces(usize),
    Empty,
    MissingSeparator(char),
    MissingField(&'static str)
}
impl Display for RingError
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Errore {:?}", self)
    }
}