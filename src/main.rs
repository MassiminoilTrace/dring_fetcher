use std::path::Path;
mod fetch;
use fetch::DringManager;
mod data_types;
mod error;
mod record;

fn main()
{
    // let args: Vec<String> = std::env::args().collect();
    // if args.len() != 4 || args[2] != "-o"
    // {
    //     eprintln!("Incorrect arguments.\n\nUsage: dring_fetcher sourcefile -o outputfile");
    //     return;
    // }

    let mut dring_m = DringManager::new();
    // Reads the config url and contacts the authoritative servers
    dring_m.add_sources_from_file(Path::new("config.txt"));

    // Fetches and combines ring1 lists to create ring2
    dring_m.fetch_ring();
    dring_m.save();
}
