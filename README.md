# Dring Rust impl

A Rust implementation of the [Dring](https://gitlab.com/binrc/dring/) format.

## Usage

Simply build using
```
cargo build --release
```

You'll find the resulting executable in `target/release/dring_fetcher`.

or run using

```
cargo run --release
```

This program reads a `config.txt` file, listing one url per line, and fetches dring data from them.
Then, saves the three files to be uploaded to your webroot.

# Copyright and License information

This is NOT public domain, make sure to respect the license terms.
You can find the license text in the COPYING file.

Copyright © 2023 Massimo Gismondi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
