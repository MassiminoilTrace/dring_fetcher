use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::path::Path;

use itertools::Itertools;

const FILENAME_SELF: &'static str = "self.txt";
const FILENAME_HOP1: &'static str = "hop1.txt";
const FILENAME_HOP2: &'static str = "hop2.txt";

#[derive(Clone, Eq)]
struct DringEntry
{
    // Mandatory fields. Ordering is mandatory.
    title: String,
    description: String,
    url: String,

    // Optional fields
    rss: Option<String>,
    tor: Option<String>,
    i2p: Option<String>
}

impl std::fmt::Display for DringEntry
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        writeln!(
            f,
            "title: {}\ndescription: {}\nurl: {}",
            self.title, self.description, self.url
        )?;
        if let Some(el) = self.rss.as_ref()
        {
            writeln!(f, "rss: {el}")?;
        }
        if let Some(el) = self.tor.as_ref()
        {
            writeln!(f, "tor: {el}")?;
        }
        if let Some(el) = self.i2p.as_ref()
        {
            writeln!(f, "i2p: {el}")?;
        }
        Ok(())
    }
}

impl DringEntry
{
    /// Receives an iterator over lines and tries to create a Dring object
    /// If it fails, returns None
    fn from_lines_iterator(iterator: &mut impl Iterator<Item = String>) -> Option<Self>
    {
        fn match_and_extract<'a>(key: &str, st: &String) -> Option<String>
        {
            if st.starts_with(&format!("{}: ", key))
            {
                return Some(st[(key.len() + 2)..].to_string());
            }
            return None;
        }
        let title = match_and_extract("title", &iterator.next()?)?;
        let description = match_and_extract("description", &iterator.next()?)?;
        let url = match_and_extract("url", &iterator.next()?)?;

        let mut dring_el = DringEntry {
            title: title.to_string(),
            description: description.to_string(),
            url: url.to_string(),
            rss: None,
            tor: None,
            i2p: None
        };

        while let Some(el) = iterator.next()
        {
            if el == ""
            {
                break;
            }
            if let Some(rss) = match_and_extract("rss", &el)
            {
                dring_el.rss = Some(rss.to_string());
            }
            else if let Some(tor) = match_and_extract("tor", &el)
            {
                dring_el.tor = Some(tor.to_string());
            }
            else if let Some(i2p) = match_and_extract("i2p", &el)
            {
                dring_el.i2p = Some(i2p.to_string());
            }
        }
        return Some(dring_el);
    }

    /// Fetches the advertised details data of a server
    /// and fills in its DringEntry
    pub fn new_from_authoritative(base_url: &str) -> Result<Self, ()>
    {
        let dring_url = format!("{}{}", base_url, FILENAME_SELF);
        let fetched_data = reqwest::blocking::get(&dring_url).unwrap();
        assert!(fetched_data.status().is_success());
        DringEntry::from_lines_iterator(
            &mut fetched_data.text().unwrap().lines().map(|s| s.to_owned())
        )
        .ok_or(())
    }
}

impl std::cmp::Ord for DringEntry
{
    fn cmp(&self, other: &Self) -> core::cmp::Ordering
    {
        self.url.cmp(&other.url)
    }
}

impl std::cmp::PartialOrd for DringEntry
{
    fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering>
    {
        Some(self.cmp(other))
    }
}

impl std::cmp::PartialEq for DringEntry
{
    fn eq(&self, other: &Self) -> bool
    {
        self.url == other.url
    }
}

/// A struct to load configuration, fetch and store
/// [DringEntry] data
pub struct DringManager
{
    ring1: Vec<DringEntry>,
    ring2: Vec<DringEntry>
}
impl DringManager
{
    pub fn new() -> Self
    {
        return DringManager {
            ring1: Vec::new(),
            ring2: Vec::new()
        };
    }

    pub fn add_sources_from_file<'a>(&mut self, filepath: &'a Path)
    {
        let f = File::open(filepath).unwrap();
        let reader = BufReader::new(f);
        self.ring1.extend(reader.lines().map(|url| {
            let url = url.unwrap();
            DringEntry::new_from_authoritative(&url)
                .expect(&format!("Could not parse data received from {url}"))
        }));
    }

    // pub fn add_single_source(url: &str, title: &str, rss: &str, tor: &str, i2p: &str)
    // {
    //     todo!()
    // }

    /// Queries your other peers to get their `hop1.txt` list,
    /// then merges them and deduplicates the result
    pub fn fetch_ring(&mut self)
    {
        self.ring1.sort();
        self.ring1.dedup();

        for source in self.ring1.iter()
        {
            let dring_url = format!("{}{}", source.url, FILENAME_HOP1);
            let fetched_data = reqwest::blocking::get(&dring_url).unwrap();
            assert!(
                fetched_data.status().is_success(),
                "Url {} is invalid, doesn't return a OK status code. {} received instead.",
                &dring_url,
                fetched_data.status()
            );

            self.ring2.extend(read_hop_list(
                &mut fetched_data.text().unwrap().lines().map(|e| e.to_owned())
            ));
        }

        // Delete ring2 servers that are already
        // present in ring1
        self.ring2
            .retain(|entry| !self.ring1.iter().any(|el| el.url == entry.url));

        // Very stupid duplicate removal
        self.ring2.sort();
        self.ring2.dedup();
    }

    /// Saves both the hop1 and hop2 files.
    ///
    /// You need to call [fetch_ring](DringManager::fetch_ring) first to
    /// populate hop2 list.
    pub fn save(&self)
    {
        let mut f = BufWriter::new(File::create(Path::new(FILENAME_HOP1)).unwrap());
        for entry in self.ring1.iter()
        {
            writeln!(f, "{}\n", entry).unwrap();
        }

        let mut f = BufWriter::new(File::create(Path::new(FILENAME_HOP2)).unwrap());
        for entry in self.ring2.iter()
        {
            writeln!(f, "{}\n", entry).unwrap();
        }
    }
}

fn read_hop_list(lines: impl Iterator<Item = String>) -> Vec<DringEntry>
{
    lines
        .batching(|lines| DringEntry::from_lines_iterator(lines))
        .collect()
}
