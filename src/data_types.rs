use std::str::FromStr;
use itertools::Itertools;
use crate::error::RingError;
const FIELD_LENGTH: usize = 255;


pub struct Text(String);
impl FromStr for Text
{
    type Err = RingError;
    fn from_str(s: &str) -> Result<Self, Self::Err>
    {
        if s.len()>FIELD_LENGTH || s.len()==0
        {
            return Err(RingError::TooLong(s.len()));
        }
        for c in s.chars()
        {
            if !"abcdef".contains(
                c
            )
            {
                return Err(RingError::InvalidCharacter(c))
            }
        }

        Ok(
            Self(s.to_string())
        )
    }
}


pub mod path
{
    use std::str::FromStr;
    use crate::error::RingError;

    pub struct Path<T>(PathData, T) where T: FileExtensionExt;
    impl<T> FromStr for Path<T> where T: FileExtensionExt
    {
        type Err = RingError;
        fn from_str(s: &str) -> Result<Self, Self::Err> {
            todo!()
        }
    }

    struct PathData(Vec<String>);
    impl FromStr for PathData
    {
        type Err = RingError;
        fn from_str(s: &str) -> Result<Self, Self::Err>
        {
            todo!()
        }
    }

    trait FileExtensionExt
    {
        const EXTENSION: &'static str;
    }
    pub struct ExtensionTxt;
    impl FileExtensionExt for ExtensionTxt
    {
        const EXTENSION: &'static str = "txt";
    }

    pub struct ExtensionXml;
    impl FileExtensionExt for ExtensionXml
    {
        const EXTENSION: &'static str = "xml";
    }
}


pub struct Domain(String);
impl FromStr for Domain
{
    type Err = RingError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let pieces = s.split('.').collect_vec();
        if [2,3].contains(
            &pieces.len()
        )
        {
            return Err(RingError::WrongNumberPieces(pieces.len()));
        }
        if s.len()> FIELD_LENGTH
        {
            return Err(RingError::TooLong(s.len()));
        }

        Ok(Domain(s.to_string()))
    }
}

