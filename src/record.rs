use std::{collections::HashMap, str::FromStr};
use crate::{data_types::{Domain, Text}, error::RingError};
use crate::data_types::path::*;

pub struct Record
{
    title: Text,
    description: Text,
    url: Domain,
    rss: Option<Path<ExtensionXml>>
}
impl FromStr for Record
{
    type Err = RingError;
    fn from_str(s: &str) -> Result<Self, Self::Err>
    {
        let mut tmp: HashMap<&str, Option<&str>> = HashMap::new();
        tmp.insert("title", None);
        tmp.insert("description", None);
        tmp.insert("url", None);
        tmp.insert("rss", None);


        for line in s.split('\n')
        {
            let (key, value) = separate_key_value(line)?;
            if let Some(inner_value) = tmp.get_mut(key)
            {
                *inner_value = Some(value);
            }
        }
        
        Ok(
            Self{
                title: {
                    Text::from_str(
                        match tmp
                        .get("title")
                        .unwrap()
                        {
                            Some(el) => el,
                            None => return Err(RingError::MissingField("title"))
                        }
                    )?
                },
                description: {
                    Text::from_str(
                        match tmp.get("description").unwrap()
                        {
                            Some(el) => el,
                            None => return Err(RingError::MissingField("description"))
                        }
                    )?
                },
                url: {
                    Domain::from_str(
                        match tmp.get("url").unwrap()
                        {
                            Some(el) => el,
                            None => return Err(RingError::MissingField("url"))
                        }
                    )?
                },
                rss: {
                    match tmp.get("rss").unwrap()
                    {
                        Some(rss_value) => {
                            Some(
                                Path::from_str(
                                    &rss_value
                                )?
                            )
                        },
                        None => None
                    }
                }
            }
        )
    }
}


fn separate_key_value(s: &str) -> Result<(&str, &str), RingError>
{
    if let Some(position) = s.find(':')
    {
        let (k,v) = s.split_at(position);
        return Ok((k.trim(), v.trim()));
    }
    else{
        return Err(RingError::MissingSeparator(':'));
    }
}
